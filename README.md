# MORE.tech - решение команды SetUp

Этот репозиторий описывает решение кейса WEB. В нем есть описание решения, ссылки на код и инструкция по установке.

## Содержание

- [MORE.tech - решение команды SetUp](#moretech---решение-команды-setup)
  - [Содержание](#содержание)
  - [Демонстрация](#демонстрация)
  - [Описание](#описание)
  - [Архитектура](#архитектура)
  - [Исходный код](#исходный-код)
  - [Установкка](#установкка)
  - [FAQ](#faq)
    - [Где найти исходный код?](#где-найти-исходный-код)
    - [Что делать, если появятся какие-либо вопросы?](#что-делать-если-появятся-какие-либо-вопросы)

## Демонстрация

TODO: вставить видео

## Описание

В современной мире IT широко распостраненной практикой является использование **облачных вычислений**, в частности, использование подхода **гибридного облака**. Такого сорта решения должны обладать следующим набором качеств:

- Использование **cloud-native** инструментов и технологий
- **Отсутствие зависимости от инфраструктуры** развертывания
- **Стойкость к изменениям** бизнес-процессов
- **Применение методологий Agile и DevOps**.

В связи с этим мы предлагаем маркетплейс данных, который обладает следующими преимуществами:

- **Современный пользовательский интерфейс**, позволяющий упростить и ускорить время сборки датасета
- **Адаптивная система биллинга**, которая зависит от используемости данных
- **Активное использование очередей**, которые позволяют решать наиболее ресурсоемкие задачи "в фоне"
- **Легкая расширяемость функционала** за счет микросервисной архитектуры
- **Слабая связанность и высокая схваченность** микросервисов за счет значительного разбиения задачи на микросервисы
- **Использование методологии DevOps** посредством реализации подходов CI/CD
- **Максимальная готовность к развертыванию в облачной среде**

## Архитектура

В качестве референсной архитектуры были использованы два подхода: **[микросервисная архитектура](https://microservices.io/patterns/microservices.html)**, **[Shared Database](https://microservices.io/patterns/data/shared-database.html)** и **[Saga паттерн](https://microservices.io/patterns/data/saga.html)**. Диаграмма сервисов, которые планируется сделать в рамках хакатона, представлена ниже:

![Архитектура микросервисов](img/Architecture.png)

Каждый блок - это отдельний микросервис, который выполняет относительно несложную задачу. Серым цветом представлены **сторонними сервисами** (о них в [описании микросервисов](#исходный-код)), а белые разработаны непосредственно нами.

## Исходный код
Итак, здесь представлена таблица с кратким описанием всех разработанных сервисов:

| Название сервиса | Краткое описание | Публичный порт | Приватный порт
| ---------------- | ---------------- | -------------- | ------------- |
| [***Frontend***](https://gitlab.com/setup-more-tech/frontend) | **Основной сервис** для коммуникации с непосредственными пользователями | 80 | 80
| [***OIDC***](https://gitlab.com/setup-more-tech/oidc) | Сервис отвечает за **аутентификацию и авторизацию**, отвечающую стандартам **OpenID**. В нашем случае мы используем слегка измененный форк репозитория [ORA hydra](https://www.ory.sh/hydra). | 4444 | 4444 |
| [***FindNewFeature***](https://gitlab.com/setup-more-tech/find-new-feature) | Сервис, который запускается в **асинхронном режиме** по отношению к открытому датасету и служит для **формирования и отправки** задачи по поиску новой фичи в данном датасете **в очередь**| 8080 | 5000
| [***PrepareDatasets***](https://gitlab.com/setup-more-tech/prepare-datasets) | Сервис, который запускается и служит для **формирования и отправки** задачи по подготовке датасета по заданным правилам **в очередь**| 8081 | 5000 |
| [***Metrics***](https://gitlab.com/setup-more-tech/metrics) | Данный сервис служит для **поиска в базе данных** текущих **показателей популярности датасетов или отдельных столбцов** определенного датасета | 8082 | 5000
| [***Billing***](https://gitlab.com/setup-more-tech/billing) | Сервис выполняет задачу расчета цены за требуемый датасет, исходя из базовой стоимости датасета и показателей популярности, полученного из сервиса Metrics | 8083 | 5000
| ***QueueService***| Этот сервис отвечает за хранение очереди сообщений, реализованной с помощью **Apache Kafka**. В нем хранятся **очереди для поиске новых фич и подготовки датасетов** | 2181 | 2181
| [***DatasetConsumer***](https://gitlab.com/setup-more-tech/dataset-consumer) | Сервис является **обработчиком сообщений в очереди подготовки датасетов**.| - | 5000
| [***FeatureConsumer***](https://gitlab.com/setup-more-tech/feature-consumer) | Сервис является **обработчиком сообщений в очереди поиска фич**. По завершении работы вызывает сервис | - | 5000
| [***DatabaseService***](https://gitlab.com/setup-more-tech/database-service)| Сервис отвечает за коммуникацию с любой из трех таблиц в объединенной базе данных. | 8086 | 5000

Каждый сервис находится в отдельном репозитории (кроме ```QueueService```, т.к. он представлен Apache Kafka), ссылка на который есть в названиях каждого сервиса из таблицы выше. Также, его можно найти в [текущей группе репозиториев](https://gitlab.com/setup-more-tech)

## Установкка

Наша текущая реализация поддерживает запуск с помощью docker-compose, поэтому вам необходимо **скачать ```docker-compose.yml```** и запустить следующую команду:

    docker-compose -f docker-compose.yml -f $OIDC_PATH/docker-compose.ml up -d

```$OIDC_PATH``` - путь к локальной копии [репозитория OIDC](https://gitlab.com/setup-more-tech/oidc). Это необходимо, так как сервис Hydra запускается с помощью своего docker-compose файла.

## FAQ

### Где найти исходный код?

Весь исходный можно просмотреть в [текущей группе репозиториев](https://gitlab.com/setup-more-tech)

### Что делать, если появятся какие-либо вопросы?

Мы с радостью примем любое предложение или замечание, поэтому можете смело писать на почту (ilya210819993@gmail.com) или Telegram (@ilya_2108).